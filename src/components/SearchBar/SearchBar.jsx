import { useState } from "react";
import s from "./style.module.css";
import { Search as SearchIcon } from "react-bootstrap-icons";

export function SearchBar({ onSubmit }) {        /*povezivanje kucanja u search sa itemima*/
  const [value, setValue] = useState("");       /*prilikom potvrde pretrage, tekst iz polja search ce se ukloniti*/
  function submit(e) {                                      /*ovo submit(e) znaci obrada podataka prilikom tipke enter u polje za unos podataka*/
    if (e.key === "Enter" && e.target.value.trim()!=="") {  /*odje provjerava prvo je li pritisnut enter, a onda gleda da vrijednos u polju za pretragu nije prazan string, i kada to bude ispunjeno onda se poziva funckija onSubmit(e.target.value)*/
      onSubmit(e.target.value);
      setValue("");               /*uklanjanje teksta*/
    }
  }

  function handleChange(e) {      /*povezano sa ovim gore za uklanjanje teksta*/
    setValue(e.target.value);
  }
  return (
    <>
      <SearchIcon size={27} className={s.icon} />
      <input
        onKeyUp={submit}
        onChange={handleChange}
        className={s.input}
        type="text"
        value={value}
        placeholder={"Search a tv show you may like"}
      />
    </>
  );
}